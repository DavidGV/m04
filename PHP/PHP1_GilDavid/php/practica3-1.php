<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf8">
    <title>TablaDeMultiplicar</title>
    <link rel="stylesheet" href="../css/practica3-1.css">
  </head>
  <body>
    <h1><?php echo "Tabla de multiplicar" ?> </h1>
    <table border="3">
      <?php
      for ($x = 0; $x <=10; $x++){
        $multiplicacio=7*$x;
        echo "<tr>";
        echo "<th>";
        echo $x;
        echo "<td>";
        echo "7 x " . "$x= " . "$multiplicacio";
        echo "</td>";
        echo "</th>";
        echo "</tr>";
      }
      ?>
    </table>
  <footer>
    <p>David Gil Vazquianez</p>
  </footer>
  </body>
</html>
