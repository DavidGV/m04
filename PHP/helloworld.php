<!DOCTYPE html>
<html lang="ca">
 <head>
  <meta charset="utf8">
  <title>PHP</title>
 </head>
 <body>
  <?php
   echo "<h1>Hello World!</h1>";
   //comentari
   /*
   comentari
   */
   $var1 = "5";
   $var2 = "7";
   echo "El resultat de multiplicar " . $var1 . " per " . $var2 . " és: " . 
   $var1*$var2;
   print_r("<br>" . $var1);
   define("PI","3.1416");
   echo "<br>" . "<p> EL valor de pi és: " . PI . "</p> <br>";
   echo "<h1>Array en PHP</h1>";
   echo "<h2>Array estàndard</h2>";
   $varArray = array(
                     'clau1'=>'CRACK',
                     'clau2'=>'FIERA',
		     'clau3'=>'MASTODONTE', 
   		     'clau32'=>array(
			   'clau4'=>'Un',
                           'clau5'=>'Dos',
			   'clau6'=>'Menos treh'
		           )
                     );
   echo "<p>" . $varArray[clau32][clau6] . "</p>";
   echo "<p>" . $varArray . "</p>";
   echo "<p><pre>";
   print_r ( $varArray);
  ?>
 </body>
</html>
